const crypto = require('crypto');
const express = require('express');
const app = express();
const Worker = require('worker_threads').Worker

app.get('/', (req, res) => {
    const worker = new Worker(function() {
        this.onmessage = function() {
            let counter = 0;
            while (counter < 1e9) {
                counter++
            }

            postMessage(counter);
        }
    })
    
    worker.onmessage = function(message) {
        console.log(message.data);
        res.send('' + message.data);
    }

    worker.postMessage();
})

app.get('/fast', (req, res) => {
    doWork(5000);
    res.send('this was fast!')
})

app.listen(3000);
