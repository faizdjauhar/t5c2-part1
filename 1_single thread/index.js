function doWork(duration) {
    const start = Date.now();
    while (Date.now() - start < duration){}
}

doWork(5000)